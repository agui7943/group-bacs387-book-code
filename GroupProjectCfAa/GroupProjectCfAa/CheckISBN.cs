﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupProjectCfAa
{
    class CheckISBN
    {
        public string ISBN { get; set; }

        public string CheckDigit(string ISBN)
        {
            int total = 0;

            for (int Z = 0; Z < 9; Z++)
                total += (10 - Z) * Int32.Parse(ISBN[Z].ToString());

            int mod = total % 11;
            int CheckDigit = 11 - mod;

            if (CheckDigit == 10)
                return ("X");
            else
                return (CheckDigit.ToString());
        }
    }
}
