﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GroupProjectCfAa
{
    /// <summary>
    /// Interaction logic for AddByAPI.xaml
    /// </summary>
    public partial class AddByAPI : Window
    {
        public AddByAPI()
        {
            InitializeComponent();
        }
        private string key = "G5DCJRBD";
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string ISBN = isbn.Text;
            string uri = string.Format(@"http://isbndb.com/api/v2/json/{0}/book/{1}", key, ISBN);
            //string uri = @"http://www.google.com";

            WebClient client = new WebClient();
            string data = client.DownloadString(uri);
            response.Text = data;

            ConvertToObject(data);
        }
        private void ConvertToObject(string data)
        {
            JObject rawbook = JObject.Parse(data);
            List<JToken> tokens = rawbook["data"].Children().ToList();
            JToken firstToken = tokens.First();

            Book databook = firstToken.ToObject<Book>();

            MyBook myBook = new MyBook(databook);
            //MessageBox.Show(databook.title);
            MessageBox.Show(myBook.BookTitle);
        }

        private void btnHome_Click(object sender, RoutedEventArgs e)
        {
            MainPage home = new MainPage();
            this.Close();
            home.Show();
        }

    }
}
