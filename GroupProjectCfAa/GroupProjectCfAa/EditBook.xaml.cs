﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GroupProjectCfAa
{
    /// <summary>
    /// Interaction logic for EditBook.xaml
    /// </summary>
    public partial class EditBook : Window
    {
        public EditBook()
        {
            InitializeComponent();
        }

        private void btnHome_Click(object sender, RoutedEventArgs e)
        {
            MainPage home = new MainPage();
            this.Close();
            home.Show();
        }
        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            Books newBooks = new Books();
            Book book = new Book();
            book.title = txtEdit.Text;
            string result = newBooks.bookSearch(book);
            if (result == ",,,,,")
            {
                lblNoBook.Content = ("Book Not Found");
                txtEdit.Clear();
                txtEdit.Focus();
            }
            else
            {
                string[] split = result.Split(',');
                txtTitle.Text = (split[0]);
                txtAuthor.Text = (split[1]);
                txtIsbn.Text = (split[2]);
                txtYear.Text = (split[3]);
                txtGenre.Text = (split[4]);
                txtPublisher.Text = (split[5]);
                newBooks.removeBook(book);
            }
            
        }
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            Books change = new Books();
            Book book = new Book();
            book.author = txtAuthor.Text;
            book.genre = txtGenre.Text;
            book.title = txtTitle.Text;
            book.year = txtYear.Text;
            book.ISBN = txtIsbn.Text;
            book.publisher = txtPublisher.Text;
            change.addBook(book);
            MessageBox.Show("Changes Saved");
            txtAuthor.Clear();
            txtGenre.Clear();
            txtIsbn.Clear();
            txtPublisher.Clear();
            txtTitle.Clear();
            txtYear.Clear();
            txtEdit.Clear();
            txtEdit.Focus();
        }

        private void txtIsbn_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
