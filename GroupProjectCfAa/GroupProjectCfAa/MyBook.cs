﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupProjectCfAa
{
    class MyBook
    {
        private Book theBook { get; set; }
        public MyBook(Book theBook)
        {
            this.theBook = theBook;
        }
        public string Isbn
        {
            get
            {
                return theBook.isbn10;
            }
            set
            {
                theBook.isbn10 = value;
            }
        }
        public string BookTitle
        {
            get
            {
                return theBook.Title;
            }
            set
            {
                theBook.Title = value;
            }
        }
        public string FirstAuthor
        {
            get
            {
                if (theBook == null) { }
                if (theBook.author_data == null) { }
                if (theBook.author_data.First() == null) { }

                return theBook.author_data.First().name;
            }
            set { theBook.author_data.First().name = value; }
        }
        
    }
}
