﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupProjectCfAa
{
    class Book
    {
        public string author;
        public string title;
        public string year;
        public string publisher;
        public string ISBN;
        public string genre;

        public string isbn13 { get; set; }
        public string isbn10 { get; set; }
        public string Title { get; set; }
        public string publisher_id { get; set; }
        public List<Author> author_data { get; set; }


        public string checkISBN(string isbn)
        {
            isbn = isbn.Remove(isbn.Length - 1);

            int sum = 0;
            string result;
            for (int i = 0; i < 9; i++)
                sum += (10 - i) * Int32.Parse(isbn[i].ToString());
     
            int rem = sum % 11;
            int digit = 11 - rem;

            if (digit == 10)
                result = "X";
            else
                result = (digit.ToString());
     
            return (isbn + result);
            
        }
    }
    
}
