﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GroupProjectCfAa
{
    /// <summary>
    /// Interaction logic for RemoveBook.xaml
    /// </summary>
    public partial class RemoveBook : Window
    {
        public RemoveBook()
        {
            InitializeComponent();
        }

        private void btnHomePage_Click(object sender, RoutedEventArgs e)
        {
            MainPage frm2 = new MainPage();
            this.Hide();
            frm2.Show();
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            Book remBook = new Book();
            Books rb = new Books();
            remBook.title = remTitle.Text;
            remBook.author = remAuthor.Text;
            rb.removeBook(remBook);
            remTitle.Clear();
            remAuthor.Clear();

        }
    }
}
