﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WebAPIAA
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private string key = "u5XcDt6JvymshS7pHGOOIH4KqKWcp1Hyrbfjsnfl4UF81ZCSyz";

        private void button_Click(object sender, RoutedEventArgs e)
        {
            string ManUnited = soccer.Text;
            string url = string.Format(@"https://heisenbug-premier-league-live-scores-v1.p.mashape.com/api/premierleague/players", key, ManUnited);

            Manager team = new Manager();
            string input = team.DownloadString(url);
            roster.Text = input;

            ConvertToObject(input);
        }

        private void ConvertToObject(string input)
        {
            
        }
    }
}
